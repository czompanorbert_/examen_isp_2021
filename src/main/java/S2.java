import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class UI extends JFrame{
    public static JTextField text1;
    public static JTextField text2;
    JButton buton;

    UI(){
        setTitle("");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200,400);
        setVisible(true);
    }

    public void init(){
        this.setLayout(null);

        text1=new JTextField("");
        text1.setBounds(50,50,100,50);

        text2=new JTextField("");
        text2.setBounds(50,150,100,50);

        buton=new JButton("Switch");
        buton.setBounds(50,250,100,50);

        buton.addActionListener(new ButonPressed());

        add(text1);add(text2);add(buton);
    }
}

class ButonPressed implements ActionListener {
    boolean butonState=false;
    String text="Salut!";

    @Override
    public void actionPerformed(ActionEvent e) {
        if (butonState){
            UI.text1.setText("");
            UI.text2.setText(text);
            butonState=!butonState;
        }
        else{
            UI.text1.setText(text);
            UI.text2.setText("");
            butonState=!butonState;
        }
    }

}

class Main {
    public static void main(String[] args) {
        new UI();
    }
}
