import java.util.ArrayList;

class Vehicle{

}

class Car extends Vehicle{
    private String color;
    Windshield windshield;
    Wheel[] wheels=new Wheel[4];

    Car(){
        windshield=new Windshield();
        for (int i=0;i<4;i++){
            wheels[i]=new Wheel();
        }
    }

    public void start(){
        System.out.println("Engine started!");
    }

    public void stop(){
        System.out.println("Engine stopped!");
    }

    public void go(){
        System.out.println("Car is moving!");
    }
}

class Windshield{

}

class CarMaker{
    Car c;

    public void makeCar(Car c){
        this.c=c;
    }
}

class Wheel{

}

class User{
    public void driveCar(Car c){
        c.start();
        c.go();
        c.stop();
    }
}

class MainS1{
    public static void main(String[] args) {
        User u=new User();
        Car c=new Car();
        u.driveCar(c);
    }
}